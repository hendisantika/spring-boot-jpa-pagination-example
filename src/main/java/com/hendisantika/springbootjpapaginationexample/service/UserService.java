package com.hendisantika.springbootjpapaginationexample.service;

import com.hendisantika.springbootjpapaginationexample.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jpa-pagination-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-05
 * Time: 14:57
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    User create(User user);

    Page<User> findUsers(Pageable pageable);
}
