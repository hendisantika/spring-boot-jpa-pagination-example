package com.hendisantika.springbootjpapaginationexample.dto.pagination;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jpa-pagination-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-05
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
@Data
public class PreviousPage {

    @JsonProperty("page_number")
    private Integer pageNumber;

    @JsonProperty("available")
    private boolean hasPrevious;

    public PreviousPage(Page page) {

        this.hasPrevious = page.hasPrevious();
        if (page.hasPrevious()) {
            Pageable previousPage = page.previousPageable();
            this.pageNumber = previousPage.getPageNumber();
        }
    }
}