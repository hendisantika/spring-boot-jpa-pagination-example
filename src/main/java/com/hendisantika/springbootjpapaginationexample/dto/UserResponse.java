package com.hendisantika.springbootjpapaginationexample.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hendisantika.springbootjpapaginationexample.dto.pagination.PaginationDetails;
import com.hendisantika.springbootjpapaginationexample.model.User;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jpa-pagination-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-05
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */
@Data
public class UserResponse {

    private List<User> data;

    @JsonProperty("pagination_details")
    private PaginationDetails paginationDetails;

    public UserResponse(Page page) {
        this.data = page.getContent();
        paginationDetails = new PaginationDetails(page);
    }
}