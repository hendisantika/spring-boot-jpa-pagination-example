package com.hendisantika.springbootjpapaginationexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJpaPaginationExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJpaPaginationExampleApplication.class, args);
    }

}

