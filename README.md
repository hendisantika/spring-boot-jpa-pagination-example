# Spring Boot : Spring Data JPA Pagination

#### Introduction
The purpose of this article is to demonstrate the **Pagination** and its related features with **Spring Boot** and **Spring Data JPA**.
Therefore in order to keep this article simple and well focused, i will discuss only the pagination related topics with **Spring Data JPA**.
Here i have assumed that you have a prior (basic) knowledge of **Spring Boot** and **Spring Data JPA** and you know how to develop a basic application.

#### Running the application
The following command can be executed to run the application.

`mvn clean spring-boot:run`

Now the sample application is up and running.

#### Creating dummy data set
You can create the dummy set of data required to run this application by making following REST api endpoint invocation.

POST    http://localhost:8080/users

![Creating dummy data set](img/init.png "Creating dummy data set")

**createUsers** :- As described above, this endpoint is used to create set of dummy data required to run this demonstration application.

Now your **users** table of the targeted database is populated with some dummy data entries.  Lets look at the paginated REST api endpoints implementation in detailed.

#### Pagination: – User specified page and page size
In here the the related page that need to be fetched and page size (number of items need to be fetched) can be specified runtime (when the REST endpoint is invoked)

GET  http://localhost:8080/users?page=0&size=5

![Pagination: – User specified page and page size](img/page.png "Pagination: – User specified page and page size")

The page number should start from zero and it may increase based on the number of records available and page size.

Here you can see that the **page** is 0 (first page)  and **size** (number of items per page)  is 5. You can invoke the above REST api endpoints by changing **page** and **size** parameters.

Lets look at the code of the method responsible for handling above REST Api invocation.
```
@GetMapping("/users")
public UserResponse getUsers(Pageable pageable)
{
   Page page = userService.findUsers(pageable);
   return new UserResponse(page);
}
```
Notice that we haven’t passed **RequestParams** to our handler method . When the endpoint **/users?page=0&size=5** is hit,
Spring would automatically resolve the **page** and **size** parameters and create a **Pageable** instance with those values .
We would then pass this **Pageable** instance to the Service layer, which would pass it to our Repository layer.

#### Pagination: – Application specified page and page size
In here the, page and page size is set by the application itself. The user does not have to provide any parameter and it is delated in the application with some pre-defined classes.

GET  http://localhost:8080/users2

Here is the method responsible for handling above REST api invocation. (Note: “**users2**“)
```
@GetMapping("/users2")
public UserResponse getUsers2()
{
 int pageNumber = 3;
 int pageSize = 2;

 Page page = userService.findUsers(PageRequest.of(pageNumber, pageSize));
 return new UserResponse(page);
}
```

Here you can see that the **page** is 3  and **size** (number of items per page)  is 2.  After invoking above endpoint, you will get the following result.

![Pagination: – Application specified page and page size](img/user2.png "Pagination: – Application specified page and page size")

#### UserRepository is not directly extended from PagingAndSortingRepository
If you look a the the source code of the **UserRepository** class, you will notice that it is not directly inherited from the **PagingAndSortingRepository**.
You might be wondering how this pagination works without extending the **PagingAndSortingRepository**.

Let me explain. **UserRepository** is extended from the **JpaRepository**.

If you examine the source code of the **JpaRepository**, you will notice that it is extended from **PagingAndSortingRepository**.
Therefore any repository which is inherited from the **JpaRepository** will have the related methods and functionalities related to pagination.

#### Pagination related details (more)

If you go through the source code, you can find three classes (application specific) that are developed to encapsulate the pagination related details.
They can be listed as follows. (please find some time to go through those classes)

* PaginationDetails
* NextPage
* PreviousPage

Those three classes help to display the paginated related details in well formatted and descriptive manner as follows.

![Pagination related details](img/page2.png "Pagination related details")

We have called the method of **PagingAndSortingRepository**:

`Page<T> findAll(Pageable pageable);`

It returns a **Page**. **Page** contains the information about pagination and actual data being retrieved.
**getContent()** method of **Page** can be used to get the data as a **List**.
In addition, **Page** has set of  methods (inherited from  Slice)  those can be used to retrieve different pagination related details.